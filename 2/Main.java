package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[][] input = {{1,2,3},{4,5,6},{7,8,9}};
        Matrix myMatrix = new Matrix(input);
        System.out.println(Arrays.deepToString(myMatrix.matrix));

        int[][] secondInput = {{0,1,2},{3,4,5},{6,7,8}};
        System.out.println(Arrays.deepToString(myMatrix.plus(new Matrix(secondInput)).matrix));

        System.out.println(Arrays.deepToString(myMatrix.minus(new Matrix(secondInput)).matrix));

        System.out.println(Arrays.deepToString(myMatrix.numMultiply(2).matrix));

        Vector myVector = new Vector(1,1,1);
        System.out.println(myVector.length());
        System.out.println(myVector.scalarProduct(new Vector(3,3,3)));
        Vector vectorProduct = myVector.vectorProduct(new Vector(3,3,3));
        System.out.println("Vector " + vectorProduct.x+ " "+vectorProduct.y+" " + vectorProduct.z);
    }
}

class Vector {
    int x;
    int y;
    int z;
    public Vector(int x,int y,int z){
        this.x =x;
        this.y =y;
        this.z =z;
    }
    public double length(){
        return Math.sqrt(Math.pow(x,2)+Math.pow(y,2)+Math.pow(z,2));
    }

    public double scalarProduct(Vector secondVector){
        return this.x*secondVector.x+this.y*secondVector.y+this.z*secondVector.z;
    }

    public Vector vectorProduct(Vector secondVector){
        return new Vector(this.y*secondVector.z-this.z*secondVector.y,this.z*secondVector.x-this.x*secondVector.z,this.x*secondVector.y-this.y*secondVector.x);
    }
}

class Matrix {
    int[][] matrix;

    public Matrix(int[][] m){
        this.matrix = m;
    }

    public Matrix plus(Matrix secondMatrix){
        int[][] newMatrix = new int[this.matrix.length][this.matrix[0].length];
        for(int i = 0; i<this.matrix.length; i++){
            for(int j = 0; j<this.matrix[i].length; j++){
                newMatrix[i][j] = this.matrix[i][j]+secondMatrix.matrix[i][j];
            }
        }
        return new Matrix(newMatrix);
    }


    public Matrix minus(Matrix secondMatrix){
        int[][] newMatrix = new int[this.matrix.length][this.matrix[0].length];
        for(int i = 0; i<this.matrix.length; i++){
            for(int j = 0; j<this.matrix[i].length; j++){
                newMatrix[i][j] = this.matrix[i][j]-secondMatrix.matrix[i][j];
            }
        }
        return new Matrix(newMatrix);
    }

    public Matrix numMultiply(int num){
        int[][] newMatrix = new int[this.matrix.length][this.matrix[0].length];
        for(int i = 0; i<this.matrix.length; i++){
            for(int j = 0; j<this.matrix[i].length; j++){
                newMatrix[i][j] = this.matrix[i][j]*num;
            }
        }
        return new Matrix(newMatrix);
    }
}
